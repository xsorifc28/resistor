package io.xsor.resistor;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.opencv.core.Core.addWeighted;
import static org.opencv.core.Core.convertScaleAbs;
import static org.opencv.core.Core.mean;
import static org.opencv.core.Core.rectangle;
import static org.opencv.highgui.Highgui.imwrite;
import static org.opencv.imgproc.Imgproc.BORDER_DEFAULT;
import static org.opencv.imgproc.Imgproc.COLOR_GRAY2RGBA;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2GRAY;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2HSV;
import static org.opencv.imgproc.Imgproc.COLOR_RGBA2RGB;
import static org.opencv.imgproc.Imgproc.Sobel;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.threshold;

public class MainActivity extends AppCompatActivity implements CvCameraViewListener2, View.OnTouchListener {

    final static String TAG = "Main Activity";

    private CameraView mOpenCvCameraView;
    private int rectangleHeight;
    private int rectangleWidth;

    private final static double RED_MAX = 22.5 / 2;
    private final static double RED_MIN = 337.5 / 2;   // Divide by 2 since HSV ranges from 0 to 180.

    private final static double ORANGE_MIN = 22.5 / 2;
    private final static double ORANGE_MAX = 52.5 / 2;

    private final static double YELLOW_MIN = 52.5 / 2;
    private final static double YELLOW_MAX = 75 / 2;

    private final static double GREEN_MIN = 75 / 2;
    private final static double GREEN_MAX = 142.5 / 2;

    private final static double CYAN_MIN = 142.5 / 2;
    private final static double CYAN_MAX = 195 / 2;

    private final static double BLUE_MIN = 195 / 2;
    private final static double BLUE_MAX = 262.5 / 2;

    private final static double PURPLE_MIN = 270 / 2;
    private final static double PURPLE_MAX = 292.5 / 2;

    private final static double MAGENTA_MIN = 292.5 / 2;
    private final static double MAGENTA_MAX = 337.5 / 2;

    private final static double BROWN_MIN = 0;
    private final static double BROWN_MAX = 37.5 / 2;

    private final static double SAT_MIN_BW = .2 * 255;
    private final static double SAT_MAX_BW = .9 * 255;

    private final static double VAL_MIN_BW = .3 * 255;

    private final static double SAT_MIN_BRN = .1 * 255;
    private final static double VAL_MIN_BRN = .2 * 255;


    double threshValue = 255;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        mOpenCvCameraView = (CameraView) findViewById(R.id.OpenCvCameraView);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setOnTouchListener(this);

        rectangleHeight = dpToPx(60);
        rectangleWidth = dpToPx(100);


        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);

    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        mOpenCvCameraView.focusOnTouch(arg1);

        return true;
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    mOpenCvCameraView.enableView();
                    //mOpenCvCameraView.flashOn(false);
                    //mOpenCvCameraView.setFlashMode(getBaseContext(),4);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onStop() {
        super.onStop();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
       /* List<Camera.Size> camResList = mOpenCvCameraView.getResolutionList();
        String res = "";

        for(Camera.Size camSize : camResList) {
            res += " "+camSize.width +"x"+camSize.height;
        }

        Log.i(TAG + "/camRes", res);
        mOpenCvCameraView.setResolution(camResList.get(1));*/

        //mOpenCvCameraView.setFlashMode(getBaseContext(),4);

    }

    public void onCameraViewStopped() {
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        Mat frame = inputFrame.rgba();
        //Mat frameRGB = new Mat();
        //Mat frameHSV = new Mat();
        Mat frameCopy = new Mat();
        //cvtColor(frame,frameRGB,COLOR_RGBA2RGB);
        //cvtColor(frameRGB, frameHSV, COLOR_RGB2HSV);

        // Get size of the frame
        double width = frame.size().width;
        double height = frame.size().height;

        // Create a copy of the frame
        frame.copyTo(frameCopy);

        // Generate top left and bottom right of alpha-rectangle window
        Point tl = new Point(width/2-rectangleWidth/2,height/2-rectangleHeight/2);
        Point br = new Point(width/2+rectangleWidth/2,height/2+rectangleHeight/2);

        // Draw the rectangle
        rectangle(frameCopy, tl, br, new Scalar(255, 255, 255), -1);

        // Add rectangle on top of image with transparency
        double alpha = 0.3;
        addWeighted(frameCopy, alpha, frame, 1.0 - alpha, 0, frameCopy);

        // Create a region of interest
        Rect roi = new Rect((int)width/2-rectangleWidth/2,(int)height/2-rectangleHeight/2,rectangleWidth,rectangleHeight);

        // Get region of interest
        Mat frameROI = new Mat(frame, roi);

        // Create a region of interest line
        int lineHeight = 1;
        Rect roiLine = new Rect(0,rectangleHeight/2,rectangleWidth,lineHeight);

        // Draw "scan line"
        // Generate top left and bottom right of alpha-rectangle window
        tl = new Point(width/2-rectangleWidth/2,height/2);
        br = new Point(width/2+rectangleWidth/2,height/2);

        rectangle(frameCopy, tl, br, new Scalar(255, 92, 205));

        // Get the line
        Mat frameROILine = new Mat(frameROI,roiLine);

        //Mat frameROILineHSV = new Mat();

        // Convert line to RGB
        // Not necessary?
        Mat frameROILineRGB = new Mat();
        cvtColor(frameROILine, frameROILineRGB,COLOR_RGBA2RGB,3);
        //cvtColor(frameROILineRGB, frameROILineHSV, COLOR_RGB2HSV, 3);

        // Save the line
        SaveImage(frameROILine,"frameROILine");


       /* String hValues = "";
        String sValues = "";
        String vValues = "";

        for(int i = 0; i < frameROILineHSV.size().width; i++ ) {
            double[] HSV = frameROILineHSV.get(0,i);
            hValues += " " + HSV[0];
            sValues += " " + HSV[1];
            vValues += " " + HSV[2];
        }

        Log.d(TAG, "hValues: " + hValues);
        Log.d("", "");
        Log.d(TAG, "sValues: " + sValues);
        Log.d("","");
        Log.d(TAG ,"vValues: " + vValues);*/


        // Calculate Gradient
        int scale = 1;
        int delta = 0;
        int ddepth = CvType.CV_16S;

        Mat frameROILineGray = new Mat();

        //GaussianBlur(frameROILine, frameROILineGray, new Size(1, 3), 0, 0, BORDER_DEFAULT);

        cvtColor(frameROILine, frameROILineGray, COLOR_RGB2GRAY);

        Mat grad_x = new Mat();
        Mat abs_grad_x = new Mat();

        int sobel_kernel = 5;

        // Gradient X
        Sobel(frameROILineGray, grad_x, ddepth, 1, 0, sobel_kernel, scale, delta, BORDER_DEFAULT);

        //double[] kernel_arr = new double[]{3,5,0,-5,-3};
        //C++: void filter2D(InputArray src, OutputArray dst, int ddepth, InputArray kernel, Point anchor=Point(-1,-1), double delta=0, int borderType=BORDER_DEFAULT )
        //Mat kernel = new Mat(new Size(1,5),CvType.CV_8S,new Scalar(kernel_arr));

        //filter2D(frameROILineGray,grad_x,ddepth,kernel);

        convertScaleAbs(grad_x, abs_grad_x);

        threshold(abs_grad_x, abs_grad_x, 127, threshValue, THRESH_BINARY);

        int boundaryStart[] = new int[(int) (abs_grad_x.size().width/2)];
        //int boundaryEnd[] = new  int[(int) (abs_grad_x.size().width/2)];
        double value, prevValue;

        int j = 0;

       for(int i = 0; i < abs_grad_x.size().width-1; i++) {

           prevValue = abs_grad_x.get(0, i)[0];
           value = abs_grad_x.get(0, i + 1)[0];

           //Log.i(TAG,"PreVal: " + prevValue + " Value: " + value);

           if (value != prevValue) {
               boundaryStart[j] = i+1;
           //} else if (value < prevValue) {
            //   boundaryEnd[j] = i;
               j++;
           }
       }

        int numBounds = getNonZeroLocation(boundaryStart);
        long val = 0;
        if(numBounds > 3) {
            String[] colors = new String[(numBounds - 2) / 2];


            for (int i = 1; i < numBounds - 2; i += 2) {
                Rect sectionRec = new Rect(new Point(boundaryStart[i], 0), new Point(boundaryStart[i + 1], 1));
                Mat sectionMat = new Mat(frameROILine, sectionRec);
           /* for(j = 0; j<sectionMat.size().width; j++) {
                Log.i("sectionMat", j + " " + sectionMat.get(0,j)[0]);
            }*/

                SaveImage(sectionMat, "sec" + i);

                Mat sectionMatHSV = new Mat();

                cvtColor(sectionMat, sectionMatHSV, COLOR_RGBA2RGB);
                cvtColor(sectionMatHSV, sectionMatHSV, COLOR_RGB2HSV);

          /*  Mat redLow = new Mat();
            Mat redUpp = new Mat();

            inRange(sectionMatHSV, redLBL, redUBL, redLow);
            inRange(sectionMatHSV, redLBU, redUBU, redUpp);

            Mat redAdd = new Mat();
            add(redLow, redUpp, redAdd);

            if(i == 1) {
                Mat saveRed = new Mat();
                sectionMatHSV.copyTo(saveRed,redAdd);
                cvtColor(saveRed, saveRed, COLOR_HSV2RGB);
                cvtColor(saveRed, saveRed, COLOR_RGB2RGBA);

                SaveImage(saveRed, "redAdd");
            }*/

                Scalar segmentMeanColor = mean(sectionMatHSV);

                Scalar lineColor = resolveColor(segmentMeanColor, i);


                // Generate top left and bottom right of alpha-rectangle window
                tl = new Point(width / 2 - rectangleWidth / 2 + (boundaryStart[i] + boundaryStart[i + 1]) / 2 + 3, height / 2 - rectangleHeight / 2);
                br = new Point(width / 2 - rectangleWidth / 2 + (boundaryStart[i] + boundaryStart[i + 1]) / 2 + 3, height / 2 + rectangleHeight / 2);

                // Draw the rectangle
                rectangle(frameCopy, tl, br, lineColor, -1);

            }
            //val = resolveResistorValue(colors);
        }



        //Log.i(TAG+"/resValue", String.valueOf(val));
        // Add rectangle on top of image with transparency
        //alpha = 1;
        //addWeighted(frameCopy, alpha, frame,1.0-alpha,0,frameCopy);

        cvtColor(abs_grad_x, abs_grad_x, COLOR_GRAY2RGBA);

        SaveImage(abs_grad_x, "abs_grad_x");

        frameROILine.push_back(abs_grad_x);

        SaveImage(frameROILine, "frameROILine_and_grad_x");

        return frameCopy;
    }

    private String color = "";


    public void SaveImage (Mat mat, String filename) {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        filename = filename + ".png";
        File file = new File(path, filename);

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGBA2BGR);

        Boolean bool;
        filename = file.toString();
        bool = imwrite(filename, mat);

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2RGBA);

        if (bool)
            Log.d(TAG, "SUCCESS Save: " + path + filename);
        else
            Log.d(TAG, "Fail writing image to external storage");
    }

    int getNonZeroLocation(int[] arr) {

        for (int i = 0; i< arr.length; i++) {
            if(arr[i] == 0) {
                return i;
            }
        }

        return 0;
    }

    public Scalar resolveColor(Scalar segmentMeanColor, int i) {
        double h = segmentMeanColor.val[0];
        double s = segmentMeanColor.val[1];
        double v = segmentMeanColor.val[2];

        Scalar lineColor = new Scalar(255, 92, 205);
        if(s >= SAT_MIN_BW && s <= SAT_MAX_BW && v >= VAL_MIN_BW) {
            // Detected some color
            if (h > RED_MIN || h <= RED_MAX) {
                color = "red";
                lineColor = new Scalar(255, 0, 0);
            } else if(h > ORANGE_MIN && h <= ORANGE_MAX) {
                color = "orange";
                lineColor = new Scalar(255, 102, 0);
            } else if(h > YELLOW_MIN && h <= YELLOW_MAX) {
                color = "orange";
                lineColor = new Scalar(255, 255, 0);
            } else if (h > GREEN_MIN && h <= GREEN_MAX) {
                color = "green";
                lineColor = new Scalar(0, 255, 0);
            } else if (h > CYAN_MIN && h <= CYAN_MAX) {
                color = "green";
                lineColor = new Scalar(0, 255, 255);
            } else if (h > BLUE_MIN && h <= BLUE_MAX) {
                color = "blue";
                lineColor = new Scalar(0, 0, 255);
            } else if (h > PURPLE_MIN && h <= PURPLE_MAX) {
                color = "purple";
                lineColor = new Scalar(148,0,211);
            } else if (h > MAGENTA_MIN && h < MAGENTA_MAX){
                color = "magenta";
                lineColor = new Scalar(255,0,255);
            }
        } else if(v > VAL_MIN_BW && s >= SAT_MAX_BW) {
            color = "white";
            lineColor = new Scalar(255,255,255);
        } else if(v < VAL_MIN_BW && s < SAT_MIN_BW) {
            // Black
            if(v > VAL_MIN_BRN && h > BROWN_MIN && h < BROWN_MAX && s > SAT_MIN_BRN) {
                color = "brown";
                lineColor = new Scalar(139,69,19);
            } else {
                color = "black";
                lineColor = new Scalar(0, 0, 0);
            }
        }

        Log.i(TAG+"/Color","Seg " + i +": " + h + " " + s + " " + v + " Color: " + color);



        return lineColor;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getBaseContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static final String[] r ={"black","brown","red", "orange", "yellow", "green", "blue", "violet", "gray", "white"};

    public long resolveResistorValue(String[] colors) {


        int[] rVal = new int[]{0};

        for (int i = 0; i<colors.length; i++) {
            for (int j = 0; j < r.length; j++) {
                if(r[j].equals(colors[i])) {
                    rVal[i] = j;
                }
            }
        }

        long a = Integer.parseInt(Integer.toString(rVal[0]) + Integer.toString(rVal[1]))*(rVal[3]+1);


        return a;
    }
}
